<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from kofejob.dreamguystech.com/template/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Jan 2023 13:06:31 GMT -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <title>KofeJob</title>

    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png')}}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{ asset('assets/css/style.css')}}">
</head>

<body>

    <div class="main-wrapper">


        @include('base.header')


        <div class="bread-crumb-bar">
            <div class="container">
                <div class="row align-items-center inner-banner">
                    <div class="col-md-12 col-12 text-center">
                        <div class="breadcrumb-list">
                            <nav aria-label="breadcrumb" class="page-breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html"><img src="assets/img/home-icon.svg" alt="Post Author"> Home</a></li>
                                    <li class="breadcrumb-item" aria-current="page">About Us</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')

        


        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <h2 class="footer-title">Office Address</h2>
                            <div class="footer-address">
                                <div class="off-address">
                                    <p class="mb-2">New York, USA (HQ)</p>
                                    <address class="mb-0">750 Sing Sing Rd, Horseheads, NY, 14845</address>
                                    <p>Call: <a href="#"><u>469-537-2410</u> (Toll-free)</a> </p>
                                </div>
                                <div class="off-address">
                                    <p class="mb-2">Sydney, Australia </p>
                                    <address class="mb-0">24 Farrar Parade COOROW WA 6515</address>
                                    <p>Call: <a href="#"><u>(08) 9064 9807</u> (Toll-free)</a> </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">Useful Links</h2>
                                <ul>
                                    <li><a href="about.html">About Us</a></li>
                                    <li><a href="blog-list.html">Blog</a></li>
                                    <li><a href="login.html">Login</a></li>
                                    <li><a href="register.html">Register</a></li>
                                    <li><a href="forgot-password.html">Forgot Password</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">Help & Support</h2>
                                <ul>
                                    <li><a href="chats.html">Chat</a></li>
                                    <li><a href="faq.html">Faq</a></li>
                                    <li><a href="review.html">Reviews</a></li>
                                    <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                    <li><a href="term-condition.html">Terms of use</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">Other Links</h2>
                                <ul>
                                    <li><a href="freelancer-dashboard.html">Freelancers</a></li>
                                    <li><a href="freelancer-portfolio.html">Freelancer Details</a></li>
                                    <li><a href="project.html">Project</a></li>
                                    <li><a href="project-details.html">Project Details</a></li>
                                    <li><a href="post-project.html">Post Project</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="footer-widget footer-menu">
                                <h2 class="footer-title">Mobile Application</h2>
                                <p>Download our App and get the latest Breaking News Alerts and latest headlines and daily articles near you.</p>
                                <div class="row g-2">
                                    <div class="col">
                                        <a href="#"><img class="img-fluid" src="assets/img/app-store.svg" alt="app-store"></a>
                                    </div>
                                    <div class="col">
                                        <a href="#"><img class="img-fluid" src="assets/img/google-play.svg" alt="google-play"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="footer-bottom">
                <div class="container">

                    <div class="copyright">
                        <div class="row">
                            <div class="col-md-6 col-lg-6">
                                <div class="copyright-text">
                                    <p class="mb-0">&copy; 2021 All Rights Reserved</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 right-text">
                                <div class="social-icon">
                                    <ul>
                                        <li><a href="#" class="icon" target="_blank"><i class="fab fa-instagram"></i> </a></li>
                                        <li><a href="#" class="icon" target="_blank"><i class="fab fa-linkedin-in"></i> </a></li>
                                        <li><a href="#" class="icon" target="_blank"><i class="fab fa-twitter"></i> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </footer>

    </div>


    <script src="{{ asset('assets/js/jquery-3.6.0.min.js')}}"></script>

    <script src="{{ asset('assets/js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{ asset('assets/js/owl.carousel.min.js')}}"></script>

    <script src="{{ asset('assets/js/slick.js')}}"></script>

    <script src="{{ asset('assets/js/script.js')}}"></script>
</body>

<!-- Mirrored from kofejob.dreamguystech.com/template/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Jan 2023 13:06:31 GMT -->

</html>