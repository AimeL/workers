<header class="header">
    <nav class="navbar navbar-expand-lg header-nav">
        <div class="navbar-header">
            <a id="mobile_btn" href="javascript:void(0);">
                <span class="bar-icon">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
            </a>
            <a href="index.html" class="navbar-brand logo">
                <img src="assets/img/logo.png" class="img-fluid" alt="Logo">
            </a>
        </div>
        <div class="main-menu-wrapper">
            <div class="menu-header">
                <a href="index.html" class="menu-logo">
                    <img src="assets/img/logo.png" class="img-fluid" alt="Logo">
                </a>
                <a id="menu_close" class="menu-close" href="javascript:void(0);">
                    <i class="fas fa-times"></i>
                </a>
            </div>
            <ul class="main-nav">
                <li class="active has-submenu">
                    <a href="/">Accueil</a>
                </li>
                <li class="has-submenu">
                    <a href="/a-propos">A Propos</a>
                </li>
                <li class="has-submenu">
                    <a href="/comment-ça-marche">Comment ça marche ?</a>
                </li>
                <li class="has-submenu">
                    <a href="/contact">Contact</a>
                </li>
            </ul>
        </div>
        <ul class="nav header-navbar-rht">
            <li><a href="register.html" class="reg-btn"><i class="fas fa-user"></i> Register</a></li>
            <li><a href="login.html" class="log-btn"><i class="fas fa-lock"></i> Login</a></li>
            <li><a href="post-project.html" class="login-btn">Post a Project </a></li>
        </ul>
    </nav>
</header>